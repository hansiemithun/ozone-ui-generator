import React, { Component } from 'react';
import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <div className="welcome">
          <p>
            Welcome to Ozone World!
            <br />
            Clean Code for Rapid Development
          </p>
          <div className="content">
            Built in <div id="heart" /> with React
          </div>
          <p>
            To get started, edit: <pre>src/App.js</pre>
          </p>
        </div>
        <div className="footer">
          Powered by:{' '}
          <a
            target="_blank"
            href="https://www.npmjs.com/package/ozone-ui-generator">
            Ozone UI Generator - 2018
          </a>
        </div>
      </div>
    );
  }
}

export default App;
