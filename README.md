## Ozone-ui-generator

is an inspiration from [Create React App](https://github.com/facebookincubator/create-react-app). Built-in love for React developers who is fond of sass as well.

## Dependencies

###

- create-react-app
- prettier
- husky
- prettify
- eslint
- hot module reload
- sass

`$ yo install ozone-ui`

## Pre-requisites

###

Install [Yeoman](http://yeoman.io)

`npm i -g yo`

## Usage

###

`yo ozone-ui`

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
